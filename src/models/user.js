const mongoose = require('mongoose');
const schema = mongoose.Schema;

let userSchema = schema({
    email: String,
    password: String,
    firstName: String,
    lastName: String,
    identification: String,
    phone: String,
});

module.exports = mongoose.model('user',userSchema);