const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const database = require('./infrastructure/connect-db');
const user_routes = require('../routes/user');
const cats_routes = require('../routes/cat');
const images_routes = require('../routes/image');

const app = express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

//Enable cors to app
app.use(cors());

app.use('/api/user', user_routes);
app.use('/api/cat', cats_routes);
app.use('/api/image', images_routes);

module.exports = app;