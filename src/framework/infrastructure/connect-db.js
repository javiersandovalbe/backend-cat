const mongoose = require('mongoose');
const environment = require('../../../environments/environment.local');

mongoose.connect(environment.MONGO_URL) 
.then(() => {
    console.log('Connection has been established successfully.')
  })
.catch(err => {
    console.log('Unable to connect to the database')
  })

  module.exports = mongoose;