const axios = require('axios');
const environment = require('../../../environments/environment.prod');

class ImageController {

    async getImageById(req, res) {
        const { id } = req.params;
        try {
          const response = await axios.get(`https://api.thecatapi.com/v1/images/search?breed_ids=${id}`);
          if(!response.data) return res.status(400).send({ message: 'Id dont exist'});
  
          return res.status(200).send(response.data);
        } catch (error) {
          return res.status(500).send({ message: 'Something wrong with request' });
        }
      }
}

module.exports = ImageController;