const axios = require('axios');
const environment = require('../../../environments/environment.prod');

class CatController {

    async getBreeds(req, res) {
        try {
          axios.defaults.headers.common['x-api-key'] = environment.API_KEY;
          const response = await axios.get(`${environment.API_CAT}${environment.BREEDS_ENDPOINT}`);
          return res.status(200).send(response.data);
        } catch (error) {
          return res.status(500).send({ message: 'Something wrong with request' });
        }
    }

    async getBreedById(req, res) {
      const { id } = req.params;
      try {
        const response = await axios.get(`${environment.API_CAT}${environment.BREEDS_ENDPOINT}/${id}`);
        if(!response.data) return res.status(400).send({ message: 'Id dont exist'});

        return res.status(200).send(response.data);
      } catch (error) {
        return res.status(500).send({ message: 'Something wrong with request' });
      }
    }
}

module.exports = CatController;