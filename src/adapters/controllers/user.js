const User = require('../../models/user');
const validator = require('validator');
const bcrypt = require('bcrypt');
const serviceToken = require('../services/token');

class UserController {

    async create(req, res) {
        const {
            email,
            password,
            firstName,
            lastName,
            identification,
            phone
        } = req.body;
        try {
            if(!validator.isEmail(email) || validator.isEmpty(password)){
                return res.status(400).send('Verify your credentials');
            }
            const encryptPass = await bcrypt.hash(password,10);
            const user = new User(
                {
                    email: email,
                    password: encryptPass,
                    firstName: firstName,
                    lastName: lastName,
                    identification: identification,
                    phone: phone
                }
            );
            user.save().then(() => res.status(201).send({
                message: 'User created successfully.',
                token: serviceToken.createToken(user)
            }));

        } catch (error) {
            return res.status(500).send('An error occurred while creating the user.');
        }
    }

    async login(req,res) {
        const {
            email,
            password
        } = req.body;
        return User.findOne({ email: email })
        .then(user => {
            let passHash = user.password;
            bcrypt.compare(password,passHash).then((hashOk) => {
                if(hashOk) {
                    const {email, firstName, lastName, identification, phone} = user;
                    res.status(200).send({ 
                        user: {
                            email, 
                            firstName, 
                            lastName, 
                            identification, 
                            phone
                        },
                        token: serviceToken.createToken(user) 
                    });
                } else {
                    res.status(200).send({ message: 'The password is incorrect' });
                }
            })
        })
        .catch(error => res.status(400).send({ message: 'The email address is not registered' }));
    }
}

module.exports = UserController;