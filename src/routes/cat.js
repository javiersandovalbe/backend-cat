const express = require('express');
const catController = require('../adapters/controllers/cat');

const router = express.Router();
const cat = new catController();
const auth = require('../adapters/middlewares/auth');

router.get('/breeds', auth.isAuth, cat.getBreeds);
router.get('/breeds/:id', auth.isAuth, cat.getBreedById);



module.exports = router;