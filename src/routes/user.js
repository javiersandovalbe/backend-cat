const express = require('express');
const userController = require('../adapters/controllers/user');

const router = express.Router();
const user = new userController();

router.post('/create', user.create);
router.post('/login', user.login);


module.exports = router;