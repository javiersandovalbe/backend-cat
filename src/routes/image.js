const express = require('express');
const imageController = require('../adapters/controllers/image');

const router = express.Router();
const image = new imageController();
const auth = require('../adapters/middlewares/auth');

router.get('/:id', auth.isAuth, image.getImageById);


module.exports = router;